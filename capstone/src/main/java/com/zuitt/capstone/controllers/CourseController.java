package com.zuitt.capstone.controllers;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseService;

    //get all courses
    @RequestMapping(value = "/courses",method = RequestMethod.GET)
    public ResponseEntity<Object> getCourse(){
        return new ResponseEntity<>(courseService.getCourse(),HttpStatus.OK);
    }

    @RequestMapping(value = "/courses",method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value = "Authorization")String stringToken, @RequestBody Course course){
        courseService.createCourse(stringToken,course);
        return new ResponseEntity<>("Course created successfully", HttpStatus.CREATED);
    }
    //Update Course
    @RequestMapping(value = "/courses/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Object> updateCourse(@PathVariable int id, @RequestHeader(value="Authorization")String stringToken,@RequestBody Course course){
        return courseService.updateCourse(id,stringToken, course);
    }
    //Delete Course
    @RequestMapping(value ="/courses/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable int id,@RequestHeader(value = "Authorization")String stringToken){
        return courseService.deleteCourse(id,stringToken);
    }
    // Get user's Courses
    @RequestMapping(value = "/myCourses",method = RequestMethod.GET)
    public ResponseEntity<Object> getMyCourses(@RequestHeader(value = "Authorization")String stringToken){
        return new ResponseEntity<>(courseService.getMyCourses(stringToken),HttpStatus.OK);
    }
}
