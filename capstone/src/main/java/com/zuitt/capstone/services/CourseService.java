package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    void createCourse(String stringToken,Course course);
    Iterable<Course> getCourse();
    //Update course
    ResponseEntity updateCourse(int id, String stringToken,  Course course);
    //Delete course
    ResponseEntity deleteCourse(int id, String stringToken);
    //get specific course of a user
    Iterable<Course> getMyCourses(String stringToken);
}
