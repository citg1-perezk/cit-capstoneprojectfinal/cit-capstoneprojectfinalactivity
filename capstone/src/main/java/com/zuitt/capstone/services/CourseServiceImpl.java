package com.zuitt.capstone.services;

import com.zuitt.capstone.config.JwtToken;
import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.CourseRepository;
import com.zuitt.capstone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtToken jwtToken;
public void createCourse(String stringToken,Course course){
    User student=userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
    Course newCourse=new Course();
    newCourse.setName(course.getName());
    newCourse.setDescription(course.getDescription());
    newCourse.setPrice(course.getPrice());
    newCourse.setUser(student);
    courseRepository.save(newCourse);
}

    public Iterable<Course> getCourse() {
        return courseRepository.findAll();
    }
    //Update Course
    public  ResponseEntity updateCourse(int id,String stringToken,  Course course){

        Course courseForUpdating =courseRepository.findById(id).get();

        String courseStudentName=courseForUpdating.getUser().getUsername();

        String authenticatedUserName=jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(courseStudentName)){
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseForUpdating.setPrice(course.getPrice());
            courseRepository.save(courseForUpdating);
            return new ResponseEntity<>("Course updated successfully",HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to edit this course.",HttpStatus.UNAUTHORIZED);
        }

    }
    //Delete Course
    public ResponseEntity deleteCourse(int id, String stringToken){
        Course courseForDeleting=courseRepository.findById(id).get();
        String courseStudentName=courseForDeleting.getUser().getUsername();
        String authenticatedUserName=jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUserName.equals(courseStudentName)){
            courseRepository.deleteById(id);
            return new ResponseEntity<>("course deleted successfully.", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }
    }
    //get user's course
    public Iterable<Course> getMyCourses(String stringToken){
        User student=userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return student.getCourses();
    }
}
